package entities

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	Id           int
	FirstName    string
	LastName     string
	Age          int
	LanguageCode string
}

func NewStudent(id int, firstName string, lastName string, age int, languageCode string) Student {
	student := Student{
		Id:           id,
		FirstName:    firstName,
		LastName:     lastName,
		Age:          age,
		LanguageCode: languageCode,
	}
	return student
}

func (s Student) String() string {
	return fmt.Sprintf("Id : %d, Firstname : %s, Lastname : %s, Age : %d, LanguageCode : %s", s.Id, s.FirstName, s.LastName, s.Age, s.LanguageCode)
}

func (s Student) Marshal() ([]byte, error) {
	return json.Marshal(s)
}
