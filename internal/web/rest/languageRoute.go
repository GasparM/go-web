// Package classification Student API.
//
//
// Terms Of Service:
//
//     Schemes: http
//     Host: localhost:8080
//     Version: 1.0.0
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta

package webrest

import (
	"encoding/json"
	"fmt"
	"internal/entities"
	"internal/persistence/languageDao"
	"io"
	"net/http"

	"github.com/gorilla/mux"
)

var daoLanguages languageDao.LanguageDaoBolt = languageDao.NewLanguageDaoBolt()

func GetLanguages(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	j, err := json.Marshal(daoLanguages.FindAll())

	if err != nil {
		fmt.Println("An error occurred while marshaling students weather in json format: ", err)
		return
	}

	fmt.Fprintf(w, "%s", j)
}

func GetLanguage(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)

	if vars["code"] == "" {
		return
	}

	language := daoLanguages.Find(vars["code"])

	if *language != (entities.Language{}) {
		j, err := json.Marshal(language)

		if err != nil {
			fmt.Println("An error occurred while marshaling students in json format: ", err)
			return
		}

		fmt.Fprintf(w, "%s", j)
	}
}

func Createlanguage(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.WriteHeader(http.StatusOK)

	language := unmarshallLanguage(r.Body)

	isCreated := daoLanguages.Create(language)

	if isCreated {
		fmt.Println("Language created")
	} else {
		fmt.Println(w, "Language code already exist")
	}
}

func ModifyLanguage(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.WriteHeader(http.StatusOK)

	language := unmarshallLanguage(r.Body)

	isCreated := daoLanguages.Update(language)

	if isCreated {
		fmt.Println("Language remplaced")
	} else {
		fmt.Println("Language not found")
	}

}

func DeleteLanguage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)

	if vars["code"] != "" {
		code := vars["code"]

		isDeleted := daoLanguages.Delete(code)

		if isDeleted {
			fmt.Println(w, "Language deleted")
		} else {
			fmt.Println(w, "Language not found")
		}
	}
}

func unmarshallLanguage(body io.Reader) entities.Language {
	decoder := json.NewDecoder(body)
	var language entities.Language
	err := decoder.Decode(&language)
	if err != nil {
		fmt.Println("An error occurred while unmarshaling language in json format: ", err)
		return entities.Language{}
	}
	return language
}
