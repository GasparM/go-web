// Package classification Student API.
//
//
// Terms Of Service:
//
//     Schemes: http
//     Host: localhost:8080
//     Version: 1.0.0
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta

package webrest

import (
	"encoding/json"
	"fmt"
	"internal/entities"
	"internal/persistence/studentDao"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var daoStudent studentDao.StudentDaoBolt = studentDao.NewStudentDaoBolt()

func GetStudents(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	j, err := json.Marshal(daoStudent.FindAll())

	if err != nil {
		fmt.Println("An error occurred while marshaling students weather in json format: ", err)
		return
	}

	fmt.Fprintf(w, "%s", j)
}

func GetStudent(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	w.WriteHeader(http.StatusOK)

	if vars["id"] == "" {
		return
	}

	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		fmt.Println("The parameter is not a number : ", err)
		return
	}

	student := daoStudent.Find(id)

	if *student != (entities.Student{}) {
		j, err := json.Marshal(student)

		if err != nil {
			fmt.Println("An error occurred while marshaling students in json format: ", err)
			return
		}

		fmt.Fprintf(w, "%s", j)

	} else {
		fmt.Println("The parameter is not a number : ", err)
		return
	}

}

func CreateStudent(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.WriteHeader(http.StatusOK)

	student := unmarshallStudent(r.Body)

	isCreated := daoStudent.Create(student)

	if isCreated {
		fmt.Println("Studen created")
	} else {
		fmt.Println(w, "Student id already exist")
	}
}

func ModifyStudent(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.WriteHeader(http.StatusOK)

	student := unmarshallStudent(r.Body)

	isModified := daoStudent.Update(student)

	if isModified {
		fmt.Println("Student remplaced")
	} else {
		fmt.Println("Student not found")
	}

}

func DeleteStudent(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	w.WriteHeader(http.StatusOK)

	if vars["id"] != "" {
		id, err := strconv.Atoi(vars["id"])

		if err != nil {
			fmt.Println("The parameter is not a number : ", err)
			return
		}

		isDeleted := daoStudent.Delete(id)

		if isDeleted {
			fmt.Println(w, "Student deleted")
		} else {
			fmt.Println(w, "Student not found")
		}
	}
}

func unmarshallStudent(body io.Reader) entities.Student {
	decoder := json.NewDecoder(body)
	var student entities.Student
	err := decoder.Decode(&student)
	if err != nil {
		fmt.Println("An error occurred while unmarshaling students in json format: ", err)
		return entities.Student{}
	}
	return student
}
