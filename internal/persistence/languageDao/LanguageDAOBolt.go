package languageDao

import (
	"encoding/json"
	"internal/entities"
	"internal/persistence/bolt"
	"sort"
)

var languages []entities.Language = []entities.Language{
	entities.NewLanguage("21", "Go"), entities.NewLanguage("12", "Python"),
}

type LanguageDaoBolt struct {
}

var myBoltLanguage bolt.MyBolt = bolt.NewMyBolt()

var bucketLanguage string = "languageBucket"

func NewLanguageDaoBolt() LanguageDaoBolt {

	myBoltLanguage.DbCreateBucket(bucketLanguage)

	for _, language := range languages {
		res, _ := json.Marshal(language)
		myBoltLanguage.DbPut(bucketLanguage, language.Code, string(res))
	}

	return LanguageDaoBolt{}
}

func (l LanguageDaoBolt) FindAll() []entities.Language {

	var languages []entities.Language

	for _, language := range myBoltLanguage.DbGetAll(bucketLanguage) {
		var lg entities.Language
		json.Unmarshal([]byte(language), &lg)
		languages = append(languages, lg)
	}

	sort.SliceStable(languages, func(i, j int) bool {
		return languages[i].Code < languages[j].Code
	})

	return languages
}

func (s LanguageDaoBolt) Find(code string) *entities.Language {
	var lg entities.Language
	language := myBoltLanguage.DbGet(bucketLanguage, string(code))
	json.Unmarshal([]byte(language), &lg)

	return &lg
}

func (s LanguageDaoBolt) Exists(code string) bool {
	lg := myBoltLanguage.DbGet(bucketLanguage, code)

	if lg == "" {
		return false
	}
	return true
}

func (s LanguageDaoBolt) Delete(code string) bool {
	if s.Exists(code) {
		return myBoltLanguage.DbDelete(bucketLanguage, string(code))
	} else {
		return false
	}
}

func (s LanguageDaoBolt) Create(language entities.Language) bool {
	if !s.Exists(language.Code) {
		languageJson, _ := json.Marshal(language)
		return myBoltLanguage.DbPut(bucketLanguage, language.Code, string(languageJson))
	}
	return false
}

func (l LanguageDaoBolt) Update(language entities.Language) bool {
	res, _ := json.Marshal(language)

	if l.Exists(language.Code) {
		return myBoltLanguage.DbPut(bucketLanguage, language.Code, string(res))
	} else {
		return false
	}
}
