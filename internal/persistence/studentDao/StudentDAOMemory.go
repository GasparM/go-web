package studentDao

import (
	"fmt"
	"internal/entities"
	"sort"
)

type StudentDaoMemory struct{}

var students []entities.Student = []entities.Student{
	entities.NewStudent(1, "Gaspar", "Missiaen", 21, "23"),
	entities.NewStudent(2, "Daurian", "Gauron", 20, "Go"),
	entities.NewStudent(4, "Christopher", "Lessirard", 20, "26"),
	entities.NewStudent(3, "Daryl", "Caruso", 20, "-2"),
}

var StudentDao = (*StudentDaoMemory)(nil)

func NewStudentDaoMemory() StudentDaoMemory {
	return StudentDaoMemory{}
}

func (s StudentDaoMemory) FindAll() []entities.Student {

	newStudents := students

	sort.SliceStable(newStudents, func(i, j int) bool {
		return newStudents[i].Id < newStudents[j].Id
	})

	fmt.Println(newStudents)

	return newStudents
}

func (s StudentDaoMemory) Find(id int) *entities.Student {
	for _, student := range students {
		if student.Id == id {
			return &student
		}
	}
	return nil
}

func (s StudentDaoMemory) Exists(id int) bool {
	for _, student := range students {
		if student.Id == id {
			return true
		}
	}
	return false
}

func (s StudentDaoMemory) Delete(id int) bool {
	for key, value := range students {
		if value.Id == id {
			students = append(students[:key], students[key+1:]...)
			return true
		}
	}
	return false
}

func (s StudentDaoMemory) Create(student entities.Student) bool {
	if !s.Exists(student.Id) {
		students = append(students, student)
		return true
	}
	return false
}

func (s StudentDaoMemory) Update(student entities.Student) bool {
	currentStudent := s.Exists(student.Id)
	if currentStudent == true {
		for key, value := range students {
			if value.Id == student.Id {
				students[key] = student
				return true
			}
		}
	}
	return false
}
