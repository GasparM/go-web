package studentDao

import (
	"encoding/json"
	"internal/entities"
	"internal/persistence/bolt"
	"sort"
)

var studentsBolt []entities.Student = []entities.Student{
	entities.NewStudent(1, "Gaspar", "Missiaen", 21, "23"),
	entities.NewStudent(2, "Daurian", "Gauron", 20, "Go"),
	entities.NewStudent(4, "Christopher", "Lessirard", 20, "26"),
	entities.NewStudent(3, "Daryl", "Caruso", 20, "-2"),
}

type StudentDaoBolt struct {
}

var myBoltStudent bolt.MyBolt = bolt.NewMyBolt()

var bucketStudent string = "studentBucket"

func NewStudentDaoBolt() StudentDaoBolt {

	myBoltStudent.DbCreateBucket(bucketStudent)

	for _, student := range studentsBolt {
		res, _ := json.Marshal(student)
		myBoltStudent.DbPut(bucketStudent, string(student.Id), string(res))
	}

	return StudentDaoBolt{}
}

func (s StudentDaoBolt) FindAll() []entities.Student {

	var students []entities.Student

	for _, student := range myBoltStudent.DbGetAll(bucketStudent) {
		var st entities.Student
		json.Unmarshal([]byte(student), &st)
		students = append(students, st)
	}

	sort.SliceStable(students, func(i, j int) bool {
		return students[i].Id < students[j].Id
	})

	return students
}

func (s StudentDaoBolt) Find(id int) *entities.Student {
	var st entities.Student
	student := myBoltStudent.DbGet(bucketStudent, string(id))
	json.Unmarshal([]byte(student), &st)

	return &st
}

func (s StudentDaoBolt) Exists(id int) bool {
	st := myBoltStudent.DbGet(bucketStudent, string(id))

	if st == "" {
		return false
	}
	return true
}

func (s StudentDaoBolt) Delete(id int) bool {
	if s.Exists(id) {
		return myBoltStudent.DbDelete(bucketStudent, string(id))
	} else {
		return false
	}
}

func (s StudentDaoBolt) Create(student entities.Student) bool {
	if !s.Exists(student.Id) {
		studentJson, _ := json.Marshal(student)
		return myBoltStudent.DbPut(bucketStudent, string(student.Id), string(studentJson))
	}
	return false
}

func (s StudentDaoBolt) Update(student entities.Student) bool {
	res, _ := json.Marshal(student)

	if s.Exists(student.Id) {
		return myBoltStudent.DbPut(bucketStudent, string(student.Id), string(res))
	} else {
		return false
	}
}
