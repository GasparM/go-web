package bolt

import (
	"fmt"
	"internal/entities"
	"log"

	"github.com/boltdb/bolt"
)

type MyBolt struct {
	db *bolt.DB
}

var students []entities.Student = []entities.Student{
	entities.NewStudent(1, "Gaspar", "Missiaen", 21, "23"),
	entities.NewStudent(2, "Daurian", "Gauron", 20, "Go"),
	entities.NewStudent(4, "Christopher", "Lessirard", 20, "26"),
	entities.NewStudent(3, "Daryl", "Caruso", 20, "-2"),
}

var database *bolt.DB

func NewMyBolt() MyBolt {

	if database != nil {
		return MyBolt{db: database}
	} else {
		db, err := bolt.Open("bolt.db", 0600, nil)

		database = db

		if err != nil {
			log.Fatal(err)
		}

		return MyBolt{db: db}
	}

}

func (b *MyBolt) dbClose() {
	b.db.Close()
}

func (b *MyBolt) dbPath() string {
	return b.db.Path()
}

func (b *MyBolt) DbCreateBucket(bucketName string) error {
	err := b.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucketName))

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	return nil
}

func (b *MyBolt) dbDeleteBucket(bucketName string) bool {
	err := b.db.Update(func(tx *bolt.Tx) error {
		err := tx.DeleteBucket([]byte(bucketName))

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return true
}

func (b *MyBolt) DbPut(bucketName string, key string, value string) bool {
	err := b.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))

		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}

		bucket.Put([]byte(key), []byte(value))
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return true

}

func (b *MyBolt) DbGet(bucketName string, key string) string {
	var value string = ""
	err := b.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))

		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}

		value = string(bucket.Get([]byte(key)))
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return value
}

func (b *MyBolt) DbDelete(bucketName string, key string) bool {
	err := b.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))

		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}

		bucket.Delete([]byte(key))
		return nil
	})

	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func (b *MyBolt) DbGetAll(bucketName string) []string {
	var values []string

	err := b.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))

		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}

		c := bucket.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			fmt.Println(v)
			values = append(values, string(v))
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return values

}
