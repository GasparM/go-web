module projet

go 1.15

require internal/entities v1.0.0

replace internal/entities => ./internal/entities

require internal/persistence v1.0.0

replace internal/persistence => ./internal/persistence

require internal/persistence/bolt v1.0.0

replace internal/persistence/bolt => ./internal/persistence/bolt

require internal/persistence/studentDao v1.0.0

replace internal/persistence/studentDao => ./internal/persistence/studentDao

require internal/persistence/languageDao v1.0.0

replace internal/persistence/languageDao => ./internal/persistence/languageDao

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/go-openapi/analysis v0.21.3 // indirect
	github.com/go-openapi/runtime v0.23.3 // indirect
	github.com/go-openapi/spec v0.20.5 // indirect
	github.com/go-swagger/go-swagger v0.29.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/spf13/viper v1.11.0 // indirect
	go.mongodb.org/mongo-driver v1.9.0 // indirect
	golang.org/x/tools v0.1.10 // indirect
	internal/web/rest v1.0.0
)

replace internal/web/rest => ./internal/web/rest
