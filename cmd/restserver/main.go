package main

import (
	webrest "internal/web/rest"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()

	// swagger:operation GET /rest/students/{id} student getStudent
	// ---
	// summary: Return an Student provided by the id.
	// description: If the student is found, student will be returned else Error Not Found (404) will be returned.
	// parameters:
	// - id: id
	//   in: path
	//   description: id of the student
	//   type: int
	//   required: true
	r.HandleFunc("/rest/students/{id}", webrest.GetStudent).Methods("GET")

	// swagger:operation GET /rest/students/ students getStudents
	// ---
	// summary: Return all Student storaged in the bucket.
	// description: If students is found, students will be returned as an array Error Not Found (404) will be returned.
	r.HandleFunc("/rest/students", webrest.GetStudents).Methods("GET")

	// swagger:operation POST /rest/students/ students createStudents
	// ---
	// summary: Create Student send by post json value.
	// description: If student can be created, student will be created in the database else Error Not Found (404) will be returned.
	r.HandleFunc("/rest/students", webrest.CreateStudent).Methods("POST")

	// swagger:operation PUT /rest/students/ students ModifyStudents
	// ---
	// summary: Modify Student send by put json value.
	// description: If student is found, student will be modify, Error Not Found (404) will be returned.
	r.HandleFunc("/rest/students", webrest.ModifyStudent).Methods("PUT")

	// swagger:operation DELETE /rest/students/ students deleteStudents
	// ---
	// summary: Delete Student send by post.
	// description: If student is found, student will be delete from the database, Error Not Found (404) will be returned.
	// parameters:
	// - id: id
	//   in: path
	//   description: id of the student
	//   type: int
	//   required: true
	r.HandleFunc("/rest/students/{id}", webrest.DeleteStudent).Methods("DELETE")

	// swagger:operation GET /rest/language/{code} language getLanguage
	// ---
	// summary: Return an Student provided by the id.
	// description: If the language is found, language will be returned else Error Not Found (404) will be returned.
	// parameters:
	// - code: code
	//   in: path
	//   description: string of the language
	//   type: string
	//   required: true
	r.HandleFunc("/rest/languages/{code}", webrest.GetLanguage).Methods("GET")

	// swagger:operation GET /rest/languages/ language getLanguages
	// ---
	// summary: Return all Language storaged in the bucket.
	// description: If languages is found, languages will be returned as an array Error Not Found (404) will be returned.
	r.HandleFunc("/rest/languages", webrest.GetLanguages).Methods("GET")

	// swagger:operation POST /rest/languages/ language createLanguages
	// ---
	// summary: Create Language send by post json value.
	// description: If language can be created, language will be created in the database else Error Not Found (404) will be returned.
	r.HandleFunc("/rest/languages", webrest.Createlanguage).Methods("POST")

	// swagger:operation PUT /rest/languages/ languages ModifyLanguage
	// ---
	// summary: Modify Language send by put json value.
	// description: If language is found, language will be modify, Error Not Found (404) will be returned.
	r.HandleFunc("/rest/languages", webrest.ModifyLanguage).Methods("PUT")

	// swagger:operation DELETE /rest/language/ language deleteLanguage
	// ---
	// summary: Delete Language send by post.
	// description: If language is found, language will be delete from the database, Error Not Found (404) will be returned.
	// parameters:
	// - code: code
	//   in: path
	//   description: string of the language
	//   type: string
	//   required: true
	r.HandleFunc("/rest/languages/{code}", webrest.DeleteLanguage).Methods("DELETE")

	http.ListenAndServe(":8080", r)

}
